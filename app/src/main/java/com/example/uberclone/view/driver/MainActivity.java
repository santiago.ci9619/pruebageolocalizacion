package com.example.uberclone.view.driver;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;

import com.example.uberclone.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements  OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocation;
    private final static int LOCATION_REQUEST_CODE = 1;
    private final static int SETTINGS_REQUEST_CODE = 2;
    private Marker mMarker;
    private Button mButtonConnect;
    private boolean mIsConnect=false;



    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult)
        {
            for (Location location : locationResult.getLocations())
            {
                if (getApplicationContext() != null)
                {
                    if (mMarker!=null)
                    {
                        mMarker.remove();//esta validación es para que no se dupliquen los íconos del gps
                    }
                    mMarker=mMap.addMarker(new MarkerOptions().position(
                            new LatLng(location.getLatitude(),location.getLongitude())
                    )
                    .title("Tu posición")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icons8_marcador_48))
                    );
                    //obteniendo la localización del usuario en tiempo real
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition
                            (
                                    new CameraPosition.Builder()
                                            .target(new LatLng(location.getLatitude(), location.getLongitude()))
                                            .zoom(15f)
                                            .build()
                            ));
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFusedLocation = LocationServices.getFusedLocationProviderClient(this);//se inicia o se detiene la ubicación del usuario


        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this::onMapReady);

        mButtonConnect=findViewById(R.id.btn_connect);
        mButtonConnect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View vista)
            {
                if (mIsConnect)
                {
                    disconnect();
                }
                else
                    {
                        startLocation();
                    }
            }
        });


        /*
        ActivityResultLauncher<Intent> alertDialog =registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result)//terminar
                    {
                        if (result.getResultCode()== Activity.RESULT_OK)
                        {
                            Intent data=result.getData();

                        }
                    }
                }

        );*/


    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        //instanciando la propiedad locationRequest
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);//botón para agregar o quitar zoom dentro del gps
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            
            return;
        }

        mLocationRequest = LocationRequest.create()
                .setInterval(100)
                .setFastestInterval(3000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(100);
        

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)//se pregunta si el usuario concedió los permisos de ubicación
            {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
                    if (gpsActivated())
                    {
                        mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                    else
                        {
                            showAlertDialogNOGPS();
                        }


                } else
                    {
                    checkLocationPermissions();//a.este else y el de más abajo es para que en caso de que el usuario rechace los permisos, se le vuelva a preguntar
                    }

            } else {
                checkLocationPermissions();//a.
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) { //este método no va a funcionar puesto que el método ShowAlert tiene un evento deprecate
                                                                                                //sin embargo, este método lo que hace es que, una vez que se activa el gps, se ejecuta el listener para dar la ubicación
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTINGS_REQUEST_CODE && gpsActivated()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }
        else
            {
                showAlertDialogNOGPS();
            }

    }

    private  void showAlertDialogNOGPS()//método para que salga una ventana de diálogo al usuario en caso tal de que tenga el gps desactivado
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this); //
        builder.setMessage("Por favor activa el gps para continuar")
                .setPositiveButton("Configuraciones", new DialogInterface.OnClickListener()//terminar
                {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {


                    }
                }).create().show();

    }




    private boolean gpsActivated()//código para validar si el usuario tiene o no el gps activado
    {
        boolean isActive=false;
        LocationManager locationManager= (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            isActive=true;
        }
        return isActive;

    }

    private void disconnect()
    {
        mButtonConnect.setText("Conectarse");
        mIsConnect=false;
        if (mFusedLocation!= null)
        {
            mFusedLocation.removeLocationUpdates(mLocationCallback);

        }
    }


    private void startLocation()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)//en este if se realiza la validación si la versión de android es mayor a la 23(marshmallow)y así obtener la ubicación del usuario
        {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED)
            {
                if (gpsActivated())//validaciones en caso de que el usuario no conceda permisos
                {
                    mButtonConnect.setText("Desconectarse");
                    mIsConnect=true;
                    mFusedLocation.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());




                }
                else
                    {
                        showAlertDialogNOGPS();
                    }

            }
            else
                {
                    checkLocationPermissions();
                }
        }
        else
            {
                if (gpsActivated())
                {
                    mFusedLocation.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    mMap.setMyLocationEnabled(true);
                }
                else
                    {
                        showAlertDialogNOGPS();
                    }

            }
    }


    private void checkLocationPermissions()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED)//se hace la validación en caso de que el usuario no acepte los permisos de ubicación
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                new AlertDialog.Builder(this)
                        .setTitle("Por favor acepta los permisos para continuar")
                        .setMessage("Esta aplicación requiere aceptar los permisos de ubicación para poder utilizarse")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int i)
                            {
                                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);//habilita los permisos para utilizar la ubicación del celular
                            }
                        })
                        .create()
                        .show();




            }
            else

                {
                    ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_REQUEST_CODE);//habilita los permisos para utilizar la ubicación del celular

                }
        }
    }



}